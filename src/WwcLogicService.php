<?php
declare(strict_types=1);

namespace JoeyStockwell\Wwc;

class WwcLogicService implements WwcLogicInterface
{
    /**
     * @var array Where structure is [int => name, 100 => '100 Widgets', 200 => '200 Widgets']
     */
    private array $packs;

    /**
     * WwcLogicService constructor.
     * @param array $widgetPackets Where structure is [int => name, 100 => '100 Widgets', 200 => '200 Widgets']
     */
    public function __construct(array $widgetPackets)
    {
        $this->setPacks($widgetPackets);
    }

    public function getPacketsFromOrderAmount(int $orderAmount): array
    {
        return $this->getMinimumPackets(
            $this->getMinimumWidgets($orderAmount)
        );
    }

    //TODO: this function could be protected/private, and a test class used for unit testing
    public function getMinimumWidgets(int $orderAmount): int
    {
        //if the amount exists as a pack, return it
        if (isset($this->packs[$orderAmount])) {
            return $orderAmount;
        }

        //does a next item in the sorted array exist? if so, this is optimal
        $nextItemWidgetAmount = $this->getNextArrayItem($orderAmount);
        $multipleWidgetPacksAmount = $this->multiplePacksAmount($orderAmount);

        //if a next item amount did not exist, return the calculated amount
        if (!$nextItemWidgetAmount) {
            return $multipleWidgetPacksAmount;
        }

        //check if next item widget is less than or equal to the alternative, if so, return its
        if ($nextItemWidgetAmount <= $multipleWidgetPacksAmount && $nextItemWidgetAmount >= $orderAmount) {
            return $nextItemWidgetAmount;
        }

        //At this point this is the best option to send back
        return $multipleWidgetPacksAmount;
    }

    //TODO: this function could be protected/private, and a test class used for unit testing
    public function getMinimumPackets(int $widgetsToSend): array
    {
        //If minimum packets to send is less than the minimum packet,
        //we know the function getMinimumWidgets() was not used prior
        $minPacksToSend = min(array_keys($this->packs));
        if ($widgetsToSend < $minPacksToSend) {
            throw new BadWidgetInputException(
                sprintf(
                    "'%s' is below the minimum pack send of '%s'",
                    $widgetsToSend,
                    $minPacksToSend
                )
            );
        }

        $widgetsLeftToSatisfy = $widgetsToSend;
        $packsToSend = [];

        do {
            //iterate over the packs, and if the pack is less than or equal to the widgets needed,
            //add it to the return payload and deduct it from the running total
            foreach ($this->packs as $packAmount => $packName) {
                if ($widgetsLeftToSatisfy >= $packAmount) {
                    $widgetsLeftToSatisfy -= $packAmount;
                    $packsToSend[] = $packName;
                    break;
                }
            }
        } while ($widgetsLeftToSatisfy > 0);

        //At this point we should have the minimum packets to satisfy the order
        return $packsToSend;
    }

    private function setPacks(array $widgetPackets)
    {
        if ($widgetPackets === []) {
            throw new BadWidgetInputException(
                'Widget Packs data cannot be empty'
            );
        }

        foreach ($widgetPackets as $widgetPacketPrice => $widgetPacketName) {
            if (!is_numeric($widgetPacketPrice) || $widgetPacketPrice <= 0) {
                throw new BadWidgetInputException(
                    "Key in array must be numeric and above 0. '{$widgetPacketPrice}' failed."
                );
            }

            if (!is_string($widgetPacketName) || $widgetPacketName === '') {
                throw new BadWidgetInputException(
                    "Value in array must be a string. '{$widgetPacketName}' failed."
                );
            }

            //TODO: check for uniqueness?
            //TODO: trim(filter_var($widgetPacketName, FILTER_SANITIZE_STRING)) ???
        }

        ksort($widgetPackets, SORT_NUMERIC);
        $this->packs = array_reverse($widgetPackets, true);
    }

    private function getNextArrayItem(int $orderAmount)
    {
        //if possible, get the one above the order amount
        $widgetsWithOrderAmount = $this->packs;
        $widgetsWithOrderAmount[$orderAmount] = 'Ordered Pack';
        $nextOne = false;
        ksort($widgetsWithOrderAmount, SORT_NUMERIC);
        foreach (array_keys($widgetsWithOrderAmount) as $pack) {
            if ($nextOne) {
                return $pack;
            }

            if ($pack === $orderAmount) {
                $nextOne = true;
            }
        }

        return false;
    }

    private function multiplePacksAmount(int $orderAmount): int
    {
        $orderLeftToSatisfy = $orderAmount;
        $widgetsToSend = 0;

        //if we get to this point, we need to try and find the optimum amount to send
        do {
            //loop over each pack, they are in order DESC
            foreach ((array_keys($this->packs)) as $packAmount) {
                //we know at this point we have widgets left to satisfy,
                //if we are the last one in the ordered array or the current iteration can satisfy the condition
                //then deduct the amount and add it to the $widgetsToSend
                if (($packAmount === array_key_last($this->packs)) || $orderLeftToSatisfy >= $packAmount) {
                    $orderLeftToSatisfy -= $packAmount;
                    $widgetsToSend += $packAmount;
                    break;
                }
            }
        } while ($orderLeftToSatisfy > 0);

        return $widgetsToSend;
    }
}
