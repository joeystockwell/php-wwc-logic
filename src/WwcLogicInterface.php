<?php
declare(strict_types=1);

namespace JoeyStockwell\Wwc;

interface WwcLogicInterface
{
    public function __construct(array $widgetPackets);

    public function getPacketsFromOrderAmount(int $orderAmount): array;

    public function getMinimumWidgets(int $orderAmount): int;

    public function getMinimumPackets(int $widgetsToSend): array;
}
