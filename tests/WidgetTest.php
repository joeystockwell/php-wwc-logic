<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use \JoeyStockwell\Wwc\WwcLogicService;
use \JoeyStockwell\Wwc\BadWidgetInputException;

final class WidgetTest extends TestCase
{
    /**
     * Default Wwc Logic with basic use case pricing
     * @var WwcLogicService
     */
    private WwcLogicService $wwcLogic;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->wwcLogic = new WwcLogicService([
            5000 => '5000 Widgets',
            2000 => '2000 Widgets',
            1000 => '1000 Widgets',
            500  => '500 Widgets',
            250  => '250 Widgets',
        ]);
    }

    public function testCanInitiate()
    {
        $this->assertInstanceOf('\JoeyStockwell\Wwc\WwcLogicService', $this->wwcLogic);
    }

    public function testIsImplementingInterface()
    {
        $this->assertInstanceOf('\JoeyStockwell\Wwc\WwcLogicInterface', $this->wwcLogic);
    }

    public function testGetMinimumWidgetsReturnsInt()
    {
        $this->assertIsInt($this->wwcLogic->getMinimumWidgets(100));
    }

    public function testGetMinimumPacketsReturnsArray()
    {
        $this->assertIsArray($this->wwcLogic->getMinimumPackets(250));
    }

    public function testGetMinimumPacksFromOrderAmountReturnsCorrectArrayCases()
    {
        $cases = [
            1     => ['250 Widgets'],
            250   => ['250 Widgets'],
            251   => ['500 Widgets'],
            501   => ['500 Widgets', '250 Widgets'],
            12001 => ['5000 Widgets', '5000 Widgets', '2000 Widgets', '250 Widgets']
        ];

        foreach ($cases as $caseOrderAmount => $casePacksToSend) {
            $packsToSend = $this->wwcLogic->getPacketsFromOrderAmount($caseOrderAmount);
            $this->assertIsArray($packsToSend);
            $this->assertEquals($casePacksToSend, $packsToSend);
        }
    }

    public function testGetMinimumWidgetsReturnsCorrectIntCases()
    {
        $cases = [
            1     => 250,
            250   => 250,
            251   => 500,
            501   => 750,
            12001 => 12250
        ];

        foreach ($cases as $caseOrderAmount => $caseAmountToSend) {
            $amountToSend = $this->wwcLogic->getMinimumWidgets($caseOrderAmount);
            $this->assertIsInt($amountToSend);
            $this->assertEquals($caseAmountToSend, $amountToSend);
        }
    }

    public function testGetMinimumPacksReturnsCorrectArrayCases()
    {
        $cases = [
            250   => ['250 Widgets'],
            500   => ['500 Widgets'],
            750   => ['500 Widgets', '250 Widgets'],
            12250 => ['5000 Widgets', '5000 Widgets', '2000 Widgets', '250 Widgets']
        ];

        foreach ($cases as $caseOrderAmount => $casePacksToSend) {
            $packsToSend = $this->wwcLogic->getMinimumPackets($caseOrderAmount);
            $this->assertIsArray($packsToSend);
            $this->assertEquals($casePacksToSend, $packsToSend);
        }
    }

    public function testGetMinimumPacksReturnsErrorCases()
    {
        $this->expectException(BadWidgetInputException::class);
        $this->wwcLogic->getMinimumPackets(249);
    }

    public function testDifferentPackAmounts()
    {
        $cases = [
            1     => ['20000 Widgets'],
            250   => ['20000 Widgets'],
            251   => ['20000 Widgets'],
            501   => ['20000 Widgets'],
            12001 => ['20000 Widgets'],
        ];

        $customWwcLogic = new WwcLogicService([
           '20000' => '20000 Widgets'
        ]);

        foreach ($cases as $caseOrderAmount => $casePacksToSend) {
            $packsToSend = $customWwcLogic->getPacketsFromOrderAmount($caseOrderAmount);
            $this->assertIsArray($packsToSend);
            $this->assertEquals($casePacksToSend, $packsToSend);
        }
    }

    public function testEmptyPackDataThrowsException()
    {
        $this->expectException(BadWidgetInputException::class);
        $customWwcLogic = new WwcLogicService([]);
    }

    public function testBadPackDataThrowsException()
    {
        $this->expectException(BadWidgetInputException::class);
        $customWwcLogic = new WwcLogicService([
            '1111' => 'Widget Packs'
        ]);

        $this->expectException(BadWidgetInputException::class);
        $customWwcLogic = new WwcLogicService([
            0 => 'Widget Packs'
        ]);

        $this->expectException(BadWidgetInputException::class);
        $customWwcLogic = new WwcLogicService([
            1000 => 123
        ]);

        $this->expectException(BadWidgetInputException::class);
        $customWwcLogic = new WwcLogicService([
            1000 => ''
        ]);
    }

    public function testWidgetPacksGetsReordered()
    {
        $customWwcLogic = new WwcLogicService([
            500  => '500 Widgets',
            1000 => '1000 Widgets',
            5000 => '5000 Widgets',
            2000 => '2000 Widgets',
            250  => '250 Widgets',
        ]);

        $cases = [
            1     => ['250 Widgets'],
            250   => ['250 Widgets'],
            251   => ['500 Widgets'],
            501   => ['500 Widgets', '250 Widgets'],
            12001 => ['5000 Widgets', '5000 Widgets', '2000 Widgets', '250 Widgets']
        ];

        foreach ($cases as $caseOrderAmount => $casePacksToSend) {
            $packsToSend = $customWwcLogic->getPacketsFromOrderAmount($caseOrderAmount);
            $this->assertIsArray($packsToSend);
            $this->assertEquals($casePacksToSend, $packsToSend);
        }
    }

    public function testDifferentScenariosCaseA()
    {
        $customWwcLogic = new WwcLogicService([
            250  => '250 Widgets',
            500  => '500 Widgets',
            300  => '300 Widgets',
            1000 => '1000 Widgets',
            2000 => '2000 Widgets',
            5000 => '5000 Widgets',
        ]);

        $cases = [
            1     => ['250 Widgets'],
            250   => ['250 Widgets'],
            251   => ['300 Widgets'],
            501   => ['500 Widgets', '250 Widgets'],
            12001 => ['5000 Widgets', '5000 Widgets', '2000 Widgets', '250 Widgets']
        ];

        foreach ($cases as $caseOrderAmount => $casePacksToSend) {
            $packsToSend = $customWwcLogic->getPacketsFromOrderAmount($caseOrderAmount);
            $this->assertIsArray($packsToSend);
            $this->assertEquals($casePacksToSend, $packsToSend);
        }
    }

    public function testDifferentScenariosCaseB()
    {
        $customWwcLogic = new WwcLogicService([
            1    => '1 Widget',
            300  => '300 Widgets',
            3000 => '3000 Widgets',
        ]);

        $cases = [
            300 => ['300 Widgets'],
            301 => ['300 Widgets', '1 Widget'],
            600 => ['300 Widgets', '300 Widgets'],
            601 => ['300 Widgets', '300 Widgets', '1 Widget'],
            602 => ['300 Widgets', '300 Widgets', '1 Widget', '1 Widget']
        ];

        foreach ($cases as $caseOrderAmount => $casePacksToSend) {
            $packsToSend = $customWwcLogic->getPacketsFromOrderAmount($caseOrderAmount);
            $this->assertIsArray($packsToSend);
            $this->assertEquals($casePacksToSend, $packsToSend);
        }
    }

    public function testDifferentScenariosCaseC()
    {
        $customWwcLogic = new WwcLogicService([
            1    => '1 Widget',
            298  => '298 Widgets',
            300  => '300 Widgets',
            1000 => '1000 Widgets',
            3000 => '3000 Widgets',
        ]);

        $cases = [
            299  => ['298 Widgets', '1 Widget'],
            300  => ['300 Widgets'],
            3001 => ['3000 Widgets', '1 Widget'],
        ];

        foreach ($cases as $caseOrderAmount => $casePacksToSend) {
            $packsToSend = $customWwcLogic->getPacketsFromOrderAmount($caseOrderAmount);
            $this->assertIsArray($packsToSend);
            $this->assertEquals($casePacksToSend, $packsToSend);
        }
    }
}
