## System requirements
* PHP 7.4

## Installing

* Due to this composer project currently being a git repository you will need to do the following in your project.

```bash
composer config repositories.php-wwc-logic git https://gitlab.com/joeystockwell/php-wwc-logic.git
composer require joeystockwell/php-wwc-logic
```

## Running Tests

In the root of the project run the following:

```bash
./vendor/bin/phpunit tests
```

## Running Coding Standards

The library adheres to PSR2 where possible, you can run the following in the root of the project:

```bash
vendor/bin/phpcs --standard=PSR2 src
```

### Example Usage

```php
<?php

//require php autoloader so this package is loaded
require_once('/vendor/autoload.php');

use JoeyStockwell\Wwc\WwcLogicService;

$wwcLogic = new WwcLogicService([
    //int => 'pack name'
    5000 => '5000 Widgets',
    2000 => '2000 Widgets',
    1000 => '1000 Widgets',
    500  => '500 Widgets',
    250  => '250 Widgets',
]);
$packsToSend = $wwcLogic->getPacketsFromOrderAmount(12001);
?>

<ol>
<?php foreach ($packsToSend as $packToSend): ?>
    <li><?=$packToSend?></li>
<?php endforeach; ?>
</ol>
```
